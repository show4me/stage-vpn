IP_P1=3.11.206.139 # stage4.1, elastic IP
IP_P2=3.11.206.139 #
IP_P3=3.11.206.139 #


DOCKER_PORT_P1=13997
SERVER_NAMES=("STAGE-1" "STAGE-2" "STAGE-3")

SSH_PORT='2222'
SSH_USER='develop'

SSH_CONNECT_TIMEOUT=10
DOCKER_API_PORT=2375

ENVT=stage
ID=xdc78shsb9
#LINE FORMAT:  'local_name'  'remote_service:remote_service_port' 'path' 'protocol'

SERVICE_NAMES=(
    'bpe'             'bpe:80' '/swagger/'  'http'
    'config'          'config:80' '/swagger/'    'http'
    'content'         'content:80' '/swagger/'  'http'
    'crowdfunding'    'crowdfunding:80' '/swagger/'  'http'
    'document-templates' 'document-templates:80' '/swagger/'  'http'
    'file-mover'      'file-mover:80' '/swagger/'  'http'
    'file-uploader'   'file-uploader:80' '/swagger/'  'http'
    'inform'          'inform:1234' '/'  'wss'
    'inform'          'inform:80' '/swagger'  'http'
    'localise'        'localise:80' '/swagger/'   'http'
    'main-front'      'main-front:80' '/'  'http'
    'media-converter' 'media-converter:80' '/swagger/'  'http'
    'messenger'       'messenger:80' '/swagger/'  'http'
    'stage.cluster-cxdfpknsitls.eu-west-2.docdb.amazonaws.com'       'stage.cluster-cxdfpknsitls.eu-west-2.docdb.amazonaws.com:27017' '/' 'mongo'
    'mysql'           'mysql:3306' '/' 'mysql'
    'notification'    'notification:80' '/swagger/'  'http'
    'oms'             'oms:80' '/swagger/'  'http'
    'payment-service' 'payment-service:80' '/swagger/'  'http'
    'pls'             'pls-v3:80' '/swagger/' 'http'
    'portainer'       'portainer_portainer:9000' '/'  'http'
    'postgres-rds'    'stage-db-restored.cxdfpknsitls.eu-west-2.rds.amazonaws.com:5432' '/' 'postgres' # Этот  рабочий
    'products'        'products:80' '/swagger/'   'http'
    'rabbit-web'      'rabbitmq:15672' '/'   'http'
    'rabbit-aws'             'b-523b28bb-8813-487a-a888-73c9f7d9f031.mq.eu-west-2.amazonaws.com:5671' '!' 'rabbit'
    'rabbit-aws-web'         'b-523b28bb-8813-487a-a888-73c9f7d9f031.mq.eu-west-2.amazonaws.com:443' '/' 'https'
    'b-523b28bb-8813-487a-a888-73c9f7d9f031.mq.eu-west-2.amazonaws.com'  'b-523b28bb-8813-487a-a888-73c9f7d9f031.mq.eu-west-2.amazonaws.com:443' '!' 'rabbit'
    'rds'             'rds:80' '/swagger/'  'http'
    'redis'           'redis:6379' '/'  'redis'
    'redis-web-interface' 'redis-web-interface:8081' '/'  'http'
    'seats-manager'  'seats-manager:80' '/swagger/' 'http'
    'sphinx'          'sphinx:9306' '/' 'sphinx'
    'sso'             'sso:80' '/swagger/'   'http'
    'ticketing'       'ticketing:80' '/swagger/'   'http'
    'statistic'       'statistic:80' '/swagger/'   'http'
    'system-service'  'system-service:80' '/swagger/'   'http'
    'ums'             'ums:80' '/swagger/'   'http'
    'payment-service-2' 'payment-service-2:80' '/swagger/'   'http'
    'uptime-kuma' 'uptime-kuma:3001' '/status/stage/' 'http'
  )