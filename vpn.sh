#!/usr/bin/env bash
# COMPLETTE LOCAL SETUP FOR PROD 5.0 SERVER
# set -euxo pipefail
source config.sh


SSH_CONNECT_TIMEOUT=5
IP_P1=$2
IP_P2=$3
IP_P3=$4
STARTING_FORWARD_PORT=${1:-13000}


DOMAIN="${ENVT}.vpn"
ARRAY_LEN=${#SERVICE_NAMES[@]}

clear_hosts() {
  if [[ "$OSTYPE" == "linux-gnu" ]] && [[ $(uname -a) == *"icrosof"* ]]; then
    sudo sed -i -r "/${ID}/d" /mnt/c/Windows/System32/drivers/etc/hosts
    sudo sed -i '/^$/d' /mnt/c/Windows/System32/drivers/etc/hosts
    sudo sed -i '/^[[:space:]]*$/d' /mnt/c/Windows/System32/drivers/etc/hosts
    sudo sed -i -r "/${ID}/d" /etc/hosts
    sudo sed -i '/^$/d' /etc/hosts
    sudo sed -i '/^[[:space:]]*$/d' /etc/hosts
  elif [[ "$OSTYPE" == "linux-gnu" ]]; then
    sudo sed -i -r "/${ID}/d" /etc/hosts
    sudo sed -i '/^$/d' /etc/hosts
    sudo sed -i '/^[[:space:]]*$/d' /etc/hosts
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    sudo sed -i '' -E "/${ID}/d" /etc/hosts
    sudo sed -i '' -E "/^$/d" /etc/hosts
  fi
}

gen_hosts() {
  HOSTS_STRING="#${ID}"
  RN=$'\n'
  LOCALHOST=$'127.0.0.1'
  for ((i = 0; i < ARRAY_LEN; i = i + 4)); do
    if [[ ${SERVICE_NAMES[i + 2]} == "!" ]]; then
      HOSTS_STRING="${HOSTS_STRING}${RN}${LOCALHOST} ${SERVICE_NAMES[i]} #${ID}"
    else
      HOSTS_STRING="${HOSTS_STRING}${RN}${LOCALHOST} ${SERVICE_NAMES[i]}.${DOMAIN} #${ID}"
    fi
  done
  cl() {
    echo -en "${HOSTS_STRING}" | sort -u
  }
  HOSTS_STRING=$(cl)
}

ssh_connect() {
  FORWARD_PORT=$STARTING_FORWARD_PORT
  SSH_ARGS=("-o" "StrictHostKeyChecking=no" "-o" "ConnectTimeout=${SSH_CONNECT_TIMEOUT}")
  echo '==========================================================='
  echo '*** The list of tunneling items HOST:PORT'
  echo '==========================================================='
  STR=""
  for ((i = 0; i < ARRAY_LEN; i = i + 4)); do
    TNAME="${SERVICE_NAMES[i]}"
    TREMT="${SERVICE_NAMES[$((i + 1))]}"
    TPATH="${SERVICE_NAMES[$((i + 2))]}"
    PROTOCOL="${SERVICE_NAMES[$((i + 3))]}"
    SSH_ARGS+=("-L")

    if [[ ${SERVICE_NAMES[i + 2]} == "!" ]]; then
      SSH_ARGS+=("${TNAME}:${FORWARD_PORT}:${TREMT}")
      STR=$(printf "%s\\\n" "${STR}//${TNAME}:${FORWARD_PORT}")
    else
      SSH_ARGS+=("${TNAME}.${DOMAIN}:${FORWARD_PORT}:${TREMT}")
      STR=$(printf "%s\\\n" "${STR}${PROTOCOL}://${TNAME}.${DOMAIN}:${FORWARD_PORT}${TPATH}")
    fi
    ((FORWARD_PORT++))
  done
  echo -e ${STR} | sort
  echo '==========================================================='
  echo
  echo "*** (Next lines are getting from a REMOTE system):"
  SSH_ARGS+=("${SSH_USER}@${ENVT_HOST}")
  SSH_ARGS+=("-p${SSH_PORT}")
  ssh "${SSH_ARGS[@]}"
}

# Delete all VPN lines from a /etc/hosts
clear_hosts
sleep 1

# Add lines to a local /etc/hosts:
gen_hosts
echo -en "\n# ${ENVT}.vpn part. ID=${ID}. Cleared automatically after end of vpn script.
${IP_P1} 1.${ENVT}.vpn #${ID}
${IP_P2} 2.${ENVT}.vpn #${ID}
${IP_P3} 3.${ENVT}.vpn #${ID}
${HOSTS_STRING}
# stand-alone containers, ${ENVT}.vpn  ID=${ID}."\  |
  if [[ "$OSTYPE" == "linux-gnu" ]] && [[ $(uname -a) == *"icrosof"* ]]; then
    sudo tee -a /mnt/c/Windows/System32/drivers/etc/hosts /etc/hosts > /dev/null
  else
    sudo tee -a /etc/hosts >/dev/null
  fi

# Search reachable host
ENVT_HOST=0
ENVT_HOST_FOUND=0
# Set variable for ping with arguments according to platform
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  pingx="ping -c1 -w2"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  pingx="ping -c1 -t2"
fi

for LOOP_ENVT_HOST in "1.${ENVT}.vpn" "2.${ENVT}.vpn" "3.${ENVT}.vpn"; do
  echo "*** (Pinging a host and check ssh service):" ${LOOP_ENVT_HOST}
  $pingx ${LOOP_ENVT_HOST}
  echo
  PING_RESULT=$?
  echo "check ssh service" | ssh -o StrictHostKeyChecking=no -o ConnectTimeout=${SSH_CONNECT_TIMEOUT} -p${SSH_PORT} ${SSH_USER}@${LOOP_ENVT_HOST} sh
  SSH_CONNECT_RESULT=$?
  if [ ${PING_RESULT} -eq 0 ] && [ ${SSH_CONNECT_RESULT} -eq 0 ]; then
    ENVT_HOST=${LOOP_ENVT_HOST}
    ENVT_HOST_FOUND=1
    break
  else
    echo "*** [WARNING] This host is unreachable:" ${LOOP_ENVT_HOST}
    echo
  fi
done
echo

if [ "${ENVT_HOST_FOUND}" -eq "0" ]; then
  echo "*** [ERROR] There is no available host to connect to! Exiting."
  clear_hosts
  exit 1
fi
echo
echo "*** (Start connecting to): " ${ENVT_HOST}
echo

# start ssh-based VPN:
# -L ${LOCAL_HOST}:${LOCAL_PORT}:${CONTAINER_NAME}:${CONTAINER_PORT} \
# ${REMOTE_USER}@${REMOTE_IP} -p${REMOTE_PORT}
ssh_connect

# Delete all VPN lines from a /etc/hosts
echo
echo "*** Enter a ROOT password to clear /etc/hosts from temporary VPN hosts (if asked): "
clear_hosts
